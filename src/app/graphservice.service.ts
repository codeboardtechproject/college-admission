import { HttpClient } from '@angular/common/http';
import { Injectable} from '@angular/core';
import { registration } from './Model/Registration';


@Injectable({
  providedIn: 'root'
})
export class GraphserviceService {
 
 private myData:any;

  setData(data: any) {
    this.myData=data;
    console.log(this.myData);
  }

  constructor(private http:HttpClient) { }

  getSem1(){   
    console.log(this.myData);
    return this.http.get< registration[]>(
      'http://localhost:8080/student/sem1/'+this.myData);

  }

  getSem2(){   
    console.log(this.myData);
    return this.http.get< registration[]>(
      'http://localhost:8080/student/sem2/'+this.myData);

  }

  getSem3(){   
    console.log(this.myData);
    return this.http.get< registration[]>(
      'http://localhost:8080/student/sem3/'+this.myData);

  }

  getSem4(){   
    console.log(this.myData);
    return this.http.get< registration[]>(
      'http://localhost:8080/student/sem4/'+this.myData);

  }

  getEmail(label:any[],data:any[]){
    console.log(label);
    console.log(data);
    const body = { label, data };
    console.log(body);
  return this.http.post('http://localhost:8080/student/gmail', body);
  }

}
