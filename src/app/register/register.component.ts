import { Component, OnInit } from '@angular/core';
import {FormControl,FormGroup,Validators} from '@angular/forms'
import { registration } from '../Model/Registration';
import { NetworkcallService } from '../networkcall.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit{
 

 
 

userRegisterationModel:registration = new registration();
 
constructor(public urlService: NetworkcallService,public router:Router){}
  
ngOnInit(): void {
  
  }
  
  
userForms:FormGroup=new FormGroup({
  firstName :new FormControl('',Validators.required),
  lastName: new FormControl('',Validators.required),
  dob: new FormControl('',[Validators.required]),
  mobile: new FormControl('',[Validators.required,Validators.minLength(10),Validators.maxLength(10),Validators.pattern('^[9,8,7,6][0-9]+$')]),
  sslc:new FormControl('',[Validators.required,Validators.maxLength(3),Validators.pattern('^[0-9]+$'),Validators.max(100)]),
  hsc:new FormControl('',[Validators.required,Validators.maxLength(4),Validators.pattern('^[0-9]+$'),Validators.max(100)]),
  email:new FormControl('',[Validators.required,Validators.email]),
  password:new FormControl('',Validators.required),
  
});


SubmitData(){
  this.urlService.createStudentRegisteration(this.userRegisterationModel).subscribe(data=>{
    this.start();
});
  
}
start() {
  this.router.navigateByUrl('/login');
 };


}
