import { Component, ElementRef, OnInit} from '@angular/core';
import { Chart } from 'chart.js';
import { GraphserviceService } from '../graphservice.service';
import jsPDF from 'jspdf';


interface Results {
  value: any;
  viewValue: any;
}
 
@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css'],
})
export class GraphComponent implements OnInit {
  
  public chart: any;
  public label: any[]=[];
  public data: any[]=[];

  constructor(public urlService: GraphserviceService) {}
 
  SemResults: Results[] = [
    { value: 1, viewValue: 'sem-1' },
    { value: 2, viewValue: 'Sem-2' },
    { value: 3, viewValue: 'Sem-3' },
    { value: 4, viewValue: 'Sem-4' },
  ];

  getStudentResult(uname: any) {
    let name = uname;
   switch (name){
    case 1:
      this.Sem1();
      break;
    case 2:
      this.Sem2();
      break;
    case 3:
      this.Sem3();
      break;
    case 4:
      this.Sem4();
      break;
   }
 
   }

  Sem1() {
    
    this.urlService.getSem1().subscribe((res) => {
    
    console.log(res);
    res.forEach((value:any)=>{
      this.label.push(value[0]) ;
      this.data.push(value[1]);
    }); 
    this.updateChart(this.label,this.data); 
    
    });
    
  }

  Sem2() {
   
    this.urlService.getSem2().subscribe((res) => {

    console.log(res);
    res.forEach((value:any)=>{
      this.label.push(value[0]) ;
      this.data.push(value[1]);
    }); 

    this.updateChart(this.label,this.data); 
    
    });
   
  }

  Sem3() {
   
    this.urlService.getSem3().subscribe((res) => {
    console.log(res);

    res.forEach((value:any)=>{
      console.log(value, "value");
      this.label.push(value[0]) ;
      this.data.push(value[1]);
    });
    this.updateChart(this.label,this.data); 
   
    });
    
  }
  Sem4() {
    
    this.urlService.getSem4().subscribe((res) => {
    res.forEach((value:any)=>{
      this.label.push(value[0]) ;
      this.data.push(value[1]);
    }) 

    this.updateChart(this.label,this.data); 
    
    });
    
  }
 
  ngOnInit() {
    this.createChart();
  }

  createChart() {
    this.chart = new Chart('lbl', {
      type: 'bar',
      data: {
        labels: [],
        datasets: [
          {
            label: 'Student-Result',
            data: [],
            backgroundColor: ['Pink', 'Green', 'Blue', 'Orange', 'Yellow'],
          },
        ],
      },
      options: {
        aspectRatio: 3,
      },
    });
  }

  updateChart(labels: any[], data: any[]) {
    if (this.chart) {
      this.chart.destroy();
      this.label=[];
      this.data=[];
    }
    this.createChart();
    this.chart.data.labels = labels;
    this.chart.data.datasets[0].data = data;
    this.chart.update();
  }

  downloadD() {
    const labels = this.chart.data.labels;
    const data = this.chart.data.datasets[0].data;

    const doc = new jsPDF();
    doc.setFontSize(12);
    doc.text('Subject\tMark', 10, 10);

    for (let i = 0; i < labels.length; i++) {
      const label = labels[i];
      const fieldData = data[i];
      const row = `${label}\t\t${fieldData}`;
      doc.text(row, 10, (i + 2) * 12);
    }

    doc.save('Result.pdf');
  }

  getsendMail(){
    const labels = this.chart.data.labels;
    const data = this.chart.data.datasets[0].data;
    console.log(labels);
    console.log(data);
    this.sendMail(labels, data);

}
  sendMail(labels: any[], data: any[]){
    console.log(labels);
    console.log(data);
    this.urlService.getEmail(labels,data).subscribe((res)=>{
      console.log(res);
    })
  }
  
}



