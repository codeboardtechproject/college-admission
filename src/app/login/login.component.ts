import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { registration } from '../Model/Registration';
import {FormControl,FormGroup,Validators} from '@angular/forms'
import { GraphserviceService } from '../graphservice.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
    
  private URLRegistration="http://localhost:8080";
  userRegisterationModel:registration = new registration();
constructor(private router: Router, private httpClient: HttpClient,private myService:GraphserviceService){}

userForms:FormGroup=new FormGroup({
  email :new FormControl('',Validators.required),
  password: new FormControl('',Validators.required),
});
submitdata(){

  this.httpClient.post(this.URLRegistration+"/student/login",this.userRegisterationModel)
       .subscribe(
        (response) => {
          console.log('successfully logged');
           this.chart();
         },(error) => {
          alert('Invalid Username and Password');
        }

      );
  }
  chart() {
    this.router.navigateByUrl('/graph');
   };


   setData(email:any){
  const data = email
  console.log(data);
  this.myService.setData(data);
   }

  }
