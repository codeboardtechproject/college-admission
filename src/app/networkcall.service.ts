import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { registration } from './Model/Registration';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NetworkcallService {
                        
  private URLRegistration="http://localhost:8080";
  constructor(private httpClient:HttpClient) { }

  createStudentRegisteration(studentRegi:registration):Observable<registration>{
    return this.httpClient.post<registration>((this.URLRegistration+"/student/registration"),studentRegi);
  }



}
